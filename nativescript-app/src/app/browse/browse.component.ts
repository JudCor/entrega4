import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as camera from "nativescript-camera";
import * as Toast from "nativescript-toast";
import * as SocialShare from "nativescript-social-share";
import { ImageSource } from "tns-core-modules/image-source/image-source";
import { Image } from "tns-core-modules/ui/image/image";
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";
import { isAndroid } from "tns-core-modules/ui/page/page";

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    showCamera(): void {
        camera.requestCameraPermissions().then(
            function sucess() {
                camera
                    .takePicture({
                        width: 300,
                        height: 300,
                        keepAspectRatio: true,
                        saveToGallery: true
                    })
                    .then((imageAsset: ImageAsset) => {
                        ImageSource.fromAsset(imageAsset).then(
                            (imageSource: ImageSource) => {
                                SocialShare.shareImage(
                                    imageSource,
                                    "Shared from Nativescript app!"
                                );
                            }
                        );
                    })
                    .catch(err => {
                        console.log("error: ", err);
                    });
            },
            function failure() {
                Toast.makeText("No se otorgo permisos").show();
            }
        );
    }
}
