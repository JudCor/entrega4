import { Action } from "@ngrx/store";

//estado
export class Favorito {
    constructor(public nombre: string) {}
}

export interface FavoritoState {
    items: Favorito[];
}

export function InitFavoritoState() {
    return {
        items: []
    };
}

//acciones
export enum FavoritoActionTypes {
    INIT = "[Favorito] Init",
    LEER_AHORA = "[Favorito] Leer ahora"
}

export class InitFavoritoAction implements Action {
    type = FavoritoActionTypes.INIT;
    constructor(public favoritos: Array<string>) {}
}

export class LeerAhoraAction implements Action {
    type = FavoritoActionTypes.LEER_AHORA;
    constructor(public fav: Favorito) {}
}

export type FavoritoTypeActions = InitFavoritoAction | LeerAhoraAction;

//reducers
export function reducersFavorito(
    state: FavoritoState,
    action: FavoritoTypeActions
): FavoritoState {
    switch (action.type) {
        case FavoritoActionTypes.INIT: {
            const favoritos: Array<string> = (action as InitFavoritoAction)
                .favoritos;

            return {
                ...state,
                items: favoritos.map(m => new Favorito(m))
            };
        }

        case FavoritoActionTypes.LEER_AHORA: {
            return {
                ...state,
                items: [...state.items, (action as LeerAhoraAction).fav]
            };
        }
    }

    return state;
}
