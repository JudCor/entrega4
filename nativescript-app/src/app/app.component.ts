import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import {
    DrawerTransitionBase,
    RadSideDrawer,
    SlideInOnTopTransition
} from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";
// let localStorage = require('nativescript-localstorage')
import * as localStorage from "nativescript-localstorage";
import { Message } from "nativescript-plugin-firebase";
import * as Toast from "nativescript-toast";
const firebase = require("nativescript-plugin-firebase");

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;
    private user: string;

    constructor(
        private router: Router,
        private routerExtensions: RouterExtensions
    ) {
        // Use the component constructor to inject services.

        localStorage.getItem("user") || localStorage.setItem("user", "L");
        this.user = localStorage.getItem("user");
    }

    ngOnInit(): void {
        this._activatedUrl = "/home";
        this._sideDrawerTransition = new SlideInOnTopTransition();

        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe(
                (event: NavigationEnd) =>
                    (this._activatedUrl = event.urlAfterRedirects)
            );
        console.log(localStorage.getItem("user"));

        firebase
            .init({
                onMessageReceivedCallback: (message: Message) => {
                    console.log("titulo:", message.title);
                    console.log("cuerpo", message.body);
                    console.log("data", JSON.stringify(message.data));
                    Toast.makeText(`Notification: ${message.title}`).show();
                },
                onPushTokenReceivedCallback: token =>
                    console.log("firebase push token: ", token)
            })
            .then(
                () => console.log("firebase.init done"),
                error => console.log(`firebase.init error ${error}`)
            );
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        this.user = localStorage.getItem("user");
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
