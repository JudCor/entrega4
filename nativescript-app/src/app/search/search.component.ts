import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";
import * as gmaps from "nativescript-google-maps-sdk";

registerElement(
    "MapView",
    () => require("nativescript-google-maps-sdk").MapView
);

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {}

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onMapReady(args) {
        let view: gmaps.MapView = args.object;
        let Marker = new gmaps.Marker();
        Marker.position = gmaps.Position.positionFromLatLng(
            17.984475,
            -51.909365
        );
        Marker.title = "Marcador";
        Marker.snippet = "Pimer marcador";
        Marker.userData = { index: 1 };
        view.addMarker(Marker);
    }
}
