import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';

import { NavigateRoutingModule } from "./list-routing.module";
import { ListComponent } from "./list.component";
import { ListDetalleComponent } from "./detalle/list-detalle.component";
import { SearchFormComponent } from "./search/list-search.component";
import { UpdateModalComponent } from "./update/list-update.component";
import { MinLengthDirective } from "../validator/minlength.validator";
import { ListFavComponent } from "./favs/list-favs.component";

@NgModule({
    imports: [NativeScriptCommonModule, NavigateRoutingModule, NativeScriptFormsModule],
    declarations: [ListComponent, ListDetalleComponent, SearchFormComponent, UpdateModalComponent, MinLengthDirective, ListFavComponent],
    schemas: [NO_ERRORS_SCHEMA] ,
    entryComponents: [UpdateModalComponent]
})
export class ListModule {}