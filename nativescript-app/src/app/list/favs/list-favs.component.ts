import { Component, OnInit } from "@angular/core";
import { ListadoService } from "~/app/domain/listado.service";
import { AppState } from "~/app/app.module";
import { Store } from "@ngrx/store";
import { LeerAhoraAction, Favorito } from "~/app/domain/fav.model";
import * as Toast from "nativescript-toast";

@Component({
    moduleId: module.id,
    selector: "ListFav",
    template: `
        <ActionBar>
            <Label text="Favs"></Label>
        </ActionBar>
        <StackLayout col="0" row="1" class="text-color" #list>
            <ListView
                [items]="this.resultados"
                class="list-group"
                separatorColor="black"
                height="100%"
            >
                <ng-template let-item="item" let-i="index">
                    <GridLayout rows="*" columns="100px,*,*">
                        <Image
                            src="res://icon"
                            height="100px"
                            col="0"
                            row="0"
                        ></Image>
                        <Label
                            [text]="item"
                            class="list-group-item "
                            col="1"
                            row="0"
                        ></Label>
                        <button text="Leer" (tap)="onItemTap(item)" col="2" row="0"></button>
                    </GridLayout>
                </ng-template>
            </ListView>
        </StackLayout>
    `
})
export class ListFavComponent implements OnInit {
    resultados: Array<string>;

    constructor(
        private listado: ListadoService,
        private store: Store<AppState>
    ) {
        this.resultados = [];
    }

    ngOnInit() {
        this.resultados = this.listado.getFavs();
        this.store
            .select(state => state.favoritos.items)
            .subscribe(data => {
                data && Toast.makeText("Agregado a leer ahora").show();
            });
    }

    onItemTap(args) {
        this.store.dispatch(
            new LeerAhoraAction(new Favorito(args))
        );
    }
}
