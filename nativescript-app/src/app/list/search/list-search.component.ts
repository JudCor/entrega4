import {
    Component,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef
} from "@angular/core";
import { View, Color } from "tns-core-modules/ui/page/page";

@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `
        <FlexboxLayout flexDirection="row">
            <TextField
                width="90%"
                #text="ngModel"
                [(ngModel)]="textFieldSearch"
                hint="search..."
                required
            ></TextField>
            <Label *ngIf="text.hasError('required')" text="*"></Label>
        </FlexboxLayout>
        <Button
            text="Search"
            (tap)="onButtonTap()"
            (longPress)="onLongPress()"
            #button
        ></Button>
    `
})
export class SearchFormComponent {
    textFieldSearch: String = "";
    @Output() search: EventEmitter<String> = new EventEmitter();
    @ViewChild("button", { static: true }) button: ElementRef;

    onButtonTap(): void {
        if (this.textFieldSearch.length > 0) {
            this.search.emit(this.textFieldSearch);
        }
    }

    onLongPress(): void {
        const buttonAni = <View>this.button.nativeElement;
        buttonAni
            .animate({
                duration: 500,
                backgroundColor: new Color("green")
            })
            .then(_ => {
                buttonAni.animate({
                    duration: 300,
                    backgroundColor: new Color("white")
                });
            });
    }
}
