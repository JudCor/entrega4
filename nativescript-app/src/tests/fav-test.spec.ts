import * as favModelTest from "../app/domain/fav.model";

// A sample Jasmine test
describe("reducersFav", function() {
    it("reduce init data", function() {
        const state: any = favModelTest.InitFavoritoState();
        const action: any = new favModelTest.InitFavoritoAction([
            "test",
            "tns",
            "android"
        ]);

        const newState = favModelTest.reducersFavorito(state, action);

        expect(newState.items.length).toEqual(3);
        expect(newState.items[0].nombre).toEqual("test");
    });
    it("reduce leer ahora", function() {
        const state: any = favModelTest.InitFavoritoState();
        const action: any = new favModelTest.LeerAhoraAction(
            new favModelTest.Favorito("npm")
        );

        const newState = favModelTest.reducersFavorito(state, action);

        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual("npm");
    });
});
